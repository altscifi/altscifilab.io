<!--

    // rem: the 'page_item_array' is built into the HTML page itself.

    number_of_images_to_display = page_item_array.length;

    last_image_index_ordinal = number_of_images_to_display - 1;
    last_image_index = page_item_array[last_image_index_ordinal];

    // ---

    function go_to_image(image_index)
    {
      // set the anchor tag ID for image display.
      location_anchor_tag = '#' + image_index;

      // update the page location to display the current image.
      document.location.href=location_anchor_tag;

      // if this is not the last item in the list,
      current_image_item_displayed = get_current_page_anchor();

      if (page_item_array.indexOf(current_image_item_displayed) < last_image_index_ordinal)
      {
       // scroll to compensate for the page header.
       window.scrollBy(0,-300);
      }
      else
      {
       // if this _is_ the last item in the list,
       // scroll to compensate for the page header.
       window.scrollBy(0,-160);
       // window.scrollBy(0,-110);
      }


      // update the current image item index.
      document.getElementById('current_image_displayed').value = image_index;
    }

    function move_one_image_to_the_left()
    {
      // alert('left arrow pressed.');

      // retrieve the current image item index.
      current_image_item_displayed = get_current_page_anchor();

      // retreat the current image index by one.
      next_image_index_to_display = page_item_array.indexOf(current_image_item_displayed) - 1;
      next_image_item_to_display = page_item_array[next_image_index_to_display];

      // if the 'next image index' variable is one (i.e. the first image to display),
      if (next_image_index_to_display < 0)
      {
	  // reset to the last image to display.
	  next_image_item_to_display = last_image_index;
      }

      // retrieve the 'next image' background image.
      next_image_div = "dd_item_" + next_image_item_to_display;

      go_to_image(next_image_item_to_display);
    }


    function move_one_image_to_the_right()
    {
      // alert('right arrow pressed.');

      // retrieve the current image item index.
      current_image_item_displayed = get_current_page_anchor();

      // advance the current image index by one.
      next_image_index_to_display = page_item_array.indexOf(current_image_item_displayed) + 1;
      next_image_item_to_display = page_item_array[next_image_index_to_display];

      // if the 'next image index' variable is
      // larger than the number of images to display on this page,
      if (next_image_index_to_display > last_image_index_ordinal)
      {
	  // reset to the first image.
	  next_image_item_to_display = page_item_array[0];
      }

      // retrieve the 'next image' background image.
      next_image_div = "dd_item_" + next_image_item_to_display;

      go_to_image(next_image_item_to_display);
    }

    // ---

    document.onkeydown=function(e)
    {
     // if (e.which == 17) isCtrl=true;

     // if(e.which == 57 && isCtrl == true)

     // if user presses the 'alt-left' keyboard combination,
     // go back one in the browser history.
     // credit: https://stackoverflow.com/a/16006607
     if (((e.which == 37) || (e.which == 38)) && e.altKey)
     {
      // alert('back?');
      history.go(-1);
      return;
     }
     else
     // if user presses the 'alt-right' keyboard combination,
     // go forward one in the browser history.
     // credit: https://stackoverflow.com/a/16006607
     if (((e.which == 39) || (e.which == 40)) && e.altKey)
     {
      // alert('forward?');
      history.go(+1);
      return;
     }

     // ---
     else
     // if user presses '->' (right arrow key),
     // or if user presses 'V' (down arrow key),
     if ((e.which == 39) || (e.which == 40))
     {
      move_one_image_to_the_right();

      return false;
     }
     else
     // if user presses '<-' (left arrow key),
     // or user presses '^' (up arrow key),
     if ((e.which == 37) || (e.which == 38))
     {
      move_one_image_to_the_left();

      return false;
     }
    }

    document.body.onload = function()
    {
     page_anchor = get_current_page_anchor();

     // alert('page_anchor: ' + page_anchor);

     if (page_anchor == '')
     {
      image_index = page_item_array[0];
     }
     else
     {
     // extract the image ordinal ID from the page anchor
     image_index = page_anchor;
     }
    }

    function get_current_page_anchor()
    {
     // retrieve the current image ID from the page anchor.
     page_anchor = window.location.hash.substr(1);
     // credit: https://stackoverflow.com/a/10076097

     if (page_anchor == '')
     {
      page_anchor = page_item_array[0];
     }

     return page_anchor;
    }

//-->
